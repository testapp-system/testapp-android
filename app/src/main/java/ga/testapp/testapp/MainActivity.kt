package ga.testapp.testapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.support.customtabs.CustomTabsIntent
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import im.delight.apprater.AppRater
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mainWebView: WebView = findViewById(R.id.mainWebView)
        mainWebView.setWebViewClient(TestAppWebView(this))
        if(savedInstanceState==null) {
            val i = intent
            val extras = i.extras
            val action = i.action

            // Check if special url is passet
            if (Intent.ACTION_SEND == action) {
                if (extras!!.containsKey(Intent.EXTRA_STREAM)) {
                    val url = i.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as Uri
                    Toast.makeText(this, url.toString(), Toast.LENGTH_SHORT).show()
                    mainWebView.loadUrl(url.toString())
                }
            } else {

                if (0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) {
                    mainWebView.loadUrl("https://dev.testapp.ga/dashboard")
                    Toast.makeText(this, R.string.use_dev_version, Toast.LENGTH_SHORT).show()
                } else {
                    mainWebView.loadUrl("https://testapp.ga/dashboard")
                }
            }
        }
        //Allowing javascript, storage, etc.
        mainWebView.settings.javaScriptEnabled=true
        mainWebView.settings.domStorageEnabled=true
        mainWebView.settings.databaseEnabled=true
        mainWebView.settings.javaScriptCanOpenWindowsAutomatically=true
        mainWebView.settings.allowFileAccess=true
        mainWebView.addJavascriptInterface(WebAppInterface(this), "Android")
        //Checking if debugging
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) {
                WebView.setWebContentsDebuggingEnabled(true)
            }
        }
        //For rating dialog
        val appRater = AppRater(this)
        appRater.setDaysBeforePrompt(3)
        appRater.setLaunchesBeforePrompt(7)
        appRater.setPhrases(R.string.rate_title, R.string.rate_explanation, R.string.rate_now, R.string.rate_later, R.string.rate_never);
        appRater.show()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        // Check if the key event was the Back button and if there's history
        if (keyCode == KeyEvent.KEYCODE_BACK && mainWebView.canGoBack()) {
            mainWebView.goBack()
            return true
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event)
    }

}

/** Instantiate the interface and set the context  */
class WebAppInterface(private val mContext: Context) {

    /** Show a toast from the web page  */
    @JavascriptInterface
    fun showToast(toast: String) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show()
    }
    @JavascriptInterface
    fun rateApp() {
        val appRater = AppRater(mContext)
        appRater.setPhrases(R.string.rate_title, R.string.rate_explanation, R.string.rate_now, R.string.rate_later, R.string.rate_never);
        appRater.demo()
    }
}
class TestAppWebView(val activity: Context) : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        //Checking if non-TestApp url
        return if(!url!!.contains(Regex("/https:.{2,6}testapp.ga"))) {
            //Creating a customTab and loading page
            val builder = CustomTabsIntent.Builder()
            builder.enableUrlBarHiding()
            builder.setShowTitle(true)
            builder.setToolbarColor(Color.TRANSPARENT)
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(this.activity, Uri.parse(url))
            true;
        } else false //Otherwise allowing navigation
    }

    override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
        super.onReceivedError(view, errorCode, description, failingUrl)
        if(view!=null){

            var htmlData = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>No Internet</title><style>img, small {display:block;margin: 0 auto;max-width: 100%;}</style></head><body><h1>No Internet</h1><p>Please connect and <a href=\""+failingUrl+"\">try again.</a></p></body></html>";

            view.loadUrl("about:blank");
            view.loadDataWithBaseURL(null,htmlData, "text/html", "UTF-8",null);
            view.invalidate();

        }
    }
}